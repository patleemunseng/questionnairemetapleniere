from flask_wtf import Form
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields.html5 import EmailField
from wtforms.fields import SubmitField,BooleanField

from app import db
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
    DataRequired
)
from app.models import Kiosques, Participant
import pdb

class AnswerPollForm(Form):
    """
    Poll From for users to register
    """
    email = EmailField(
    'Email', validators=[InputRequired(),
                         Length(1, 64),
                         Email()])

    def validate_email(form, field):
        pdb.set_trace()
        if "capgemini" not in field.data:
            raise ValidationError("Veuillez utiliser votre compte capgemini.")

    presentor_1 = BooleanField('Présentateur', default=False)
    presentor_2 = BooleanField('Présentateur', default=False)
    presentor_3 = BooleanField('Présentateur', default=False)
    presentor_4 = BooleanField('Présentateur', default=False)
    #presentor_5 = BooleanField('Présentateur', default=False)

    kiosque_1 = QuerySelectField(
    'Rotation 1 (9h30-10h00)',
    validators=[InputRequired()],
    get_label='name_places',
    allow_blank=True,
    blank_text=u'Veuillez choisir...',
    query_factory=lambda: db.session.query(Kiosques).filter(Kiosques.places>0 ).filter_by(creneau=1).order_by("name"))

    kiosque_2 = QuerySelectField(
    'Rotation 2 (10h05-10h35)',
    validators=[InputRequired()],
    get_label='name_places',
    allow_blank=True,
    blank_text=u'Veuillez choisir...',
    query_factory=lambda: db.session.query(Kiosques).filter_by(creneau=2).filter(Kiosques.places>0 ).order_by("name"))

    kiosque_3 = QuerySelectField(
    'Rotation 3 (10h40-11h10)',
    validators=[InputRequired()],
    get_label='name_places',
    allow_blank=True,
    blank_text=u'Veuillez choisir...',
    query_factory=lambda: db.session.query(Kiosques).filter_by(creneau=3).filter(Kiosques.places>0 ).order_by("name"))

    kiosque_4 = QuerySelectField(
    'Rotation 4 (11h15-11h45)',
    validators=[InputRequired()],
    get_label='name_places',
    allow_blank=True,
    blank_text=u'Veuillez choisir...',
    query_factory=lambda: db.session.query(Kiosques).filter_by(creneau=4).filter(Kiosques.places>0 ).order_by("name"))

    #kiosque_5 = QuerySelectField(
    #'Rotation 5 (11h30-12h00)',
    #validators=[InputRequired()],
    #get_label='name_places',
    #allow_blank=True,
    #blank_text=u'Veuillez choisir...',
    #query_factory=lambda: db.session.query(Kiosques).filter_by(creneau=5).filter(Kiosques.places>0 ).order_by("name"))

    submit = SubmitField('Valider')
    submit_2 = SubmitField('Afficher tes choix')

    def validate_email(self, field):
        if Participant.query.filter_by(email=field.data).first():
            pass
