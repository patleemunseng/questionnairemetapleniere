from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_rq import get_queue

from app.models import EditableHTML
from app.models import Participant, Kiosques, get_kiosque_text, get_kiosque_venue
from app.email import send_email
from app.main.forms import AnswerPollForm
from app import db
from app.utils import checkNone, create_place_names, get_name_from_email

import pdb

main = Blueprint('main', __name__)
N_KIOSQUE = 4

@main.route('/', methods=['POST','GET'])
@main.route('/index', methods=['POST','GET'])
def index():

    """Invites a new user to create an account and set their own password."""
    form = AnswerPollForm()
    form.validate_email(form.email)

    if form.validate_on_submit() :
        kiosque_1_id = checkNone(form.kiosque_1.data)
        kiosque_2_id = checkNone(form.kiosque_2.data)
        kiosque_3_id = checkNone(form.kiosque_3.data)
        kiosque_4_id = checkNone(form.kiosque_4.data)
        #kiosque_5_id = checkNone(form.kiosque_5.data)

        kiosque_1_text = get_kiosque_text(kiosque_1_id)
        kiosque_2_text = get_kiosque_text(kiosque_2_id)
        kiosque_3_text = get_kiosque_text(kiosque_3_id)
        kiosque_4_text = get_kiosque_text(kiosque_4_id)
        #kiosque_5_text = get_kiosque_text(kiosque_5_id)

        kiosque_1_venue = get_kiosque_venue(kiosque_1_id)
        kiosque_2_venue = get_kiosque_venue(kiosque_2_id)
        kiosque_3_venue = get_kiosque_venue(kiosque_3_id)
        kiosque_4_venue = get_kiosque_venue(kiosque_4_id)
        #kiosque_5_venue = get_kiosque_venue(kiosque_5_id)


        if form.submit.data:
            for i in range(N_KIOSQUE):

                kiosque_n = "kiosque_%d" %(i+1)
                presentor_n = "presentor_%d" %(i+1)

                if form[kiosque_n].data is not None:
                    kiosque_n = Kiosques.query.filter_by(id = form[kiosque_n].data.id).first()

                    if not form[presentor_n].data:
                        kiosque_n.places = kiosque_n.places - 1
                        #if kiosque_n.places == 0:
                            #raise AttributeError("Plus de places")
                        kiosque_n.name_places = create_place_names(kiosque_n.name,
                                                                    kiosque_n.sub_name,
                                                                    kiosque_n.places)
                        db.session.add(kiosque_n)
                    else:
                        # dummy variable for comparison
                        temp = kiosque_n.n_presentor_occupied + 1

                        if temp>kiosque_n.n_presentor:
                            form = AnswerPollForm()
                            flash('Max présentateur atteint pour la session {}, merci de refaire tes choix'.format(kiosque_n.sub_name), 'form-error')

                            return render_template('main/poll.html',
                                                    form=form)
                        else:
                            kiosque_n.n_presentor_occupied = temp
                            db.session.add(kiosque_n)

            db.session.commit()
            participant = Participant.query.filter_by(email=form.email.data).first()

            if not participant:
                participant = Participant(
                    email=form.email.data,
                    presentor_1=form.presentor_1.data,
                    presentor_2=form.presentor_2.data,
                    presentor_3=form.presentor_3.data,
                    presentor_4=form.presentor_4.data,
                    #presentor_5=form.presentor_5.data,
                    kiosque_1=kiosque_1_id,
                    kiosque_2=kiosque_2_id,
                    kiosque_3=kiosque_3_id,
                    kiosque_4=kiosque_4_id,
                    #kiosque_5=kiosque_5_id,
                    )
                db.session.add(participant)
                db.session.commit()

            else:
                doublon_kiosques = [participant.kiosque_1, participant.kiosque_2,
                                    participant.kiosque_3, participant.kiosque_4]
                                    #participant.kiosque_5]

                doublon_presentors = [participant.presentor_1, participant.presentor_2,
                                    participant.presentor_3, participant.presentor_4]
                                    #participant.presentor_5]

                for i in range(N_KIOSQUE):
                    doublon_n = doublon_kiosques[i]
                    presentor_n = doublon_presentors[i]

                    if not doublon_n == 0:
                        kiosque_n = Kiosques.query.filter_by(id = doublon_n).first()

                        if not presentor_n :
                            kiosque_n.places = kiosque_n.places + 1
                            kiosque_n.name_places = create_place_names(kiosque_n.name,
                                                    kiosque_n.sub_name, kiosque_n.places)

                        else:
                            kiosque_n.n_presentor_occupied -= 1

                        db.session.add(kiosque_n)

                # update new states
                participant.kiosque_1 = kiosque_1_id
                participant.kiosque_2 = kiosque_2_id
                participant.kiosque_3 = kiosque_3_id
                participant.kiosque_4 = kiosque_4_id
                #participant.kiosque_5 = kiosque_5_id
                participant.presentor_1 = form.presentor_1.data
                participant.presentor_2 = form.presentor_2.data
                participant.presentor_3 = form.presentor_3.data
                participant.presentor_4 = form.presentor_4.data
                #participant.presentor_5 = form.presentor_5.data

                db.session.add(participant)
                db.session.commit()

            part_kiosques = [participant.kiosque_1, participant.kiosque_2,
                                participant.kiosque_3, participant.kiosque_4]
                                #participant.kiosque_5]
            kiosque_text = []
            kiosque_venue = []
            heures = ["09h30-10h00","10h05-10h35","10h40-11h10",
                    "11h15-11h45"]

            for i in range(len(part_kiosques)):
                kiosque_text.append(get_kiosque_text(part_kiosques[i]))
                kiosque_venue.append(get_kiosque_venue(part_kiosques[i]))

            flash('Merci {}, tes choix ont bien été pris en compte'.format(participant.email), 'form-success')
            #get_queue().enqueue(
            send_email(
                recipient=participant.email,
                subject='Récapitulatif de tes choix',
                template='account/email/summary',
                user=get_name_from_email(participant.email),
                kiosque_text = kiosque_text,
                kiosque_venue = kiosque_venue,
                n_element = len(part_kiosques),
                heures= heures
            )

        if form.submit_2.data:
            doublon = Participant.query.filter_by(email=form.email.data).first()

            if doublon :
                kiosque_1_text = get_kiosque_text(doublon.kiosque_1)
                kiosque_2_text = get_kiosque_text(doublon.kiosque_2)
                kiosque_3_text = get_kiosque_text(doublon.kiosque_3)
                kiosque_4_text = get_kiosque_text(doublon.kiosque_4)
                #kiosque_5_text = get_kiosque_text(doublon.kiosque_5)
                kiosque_1_venue = get_kiosque_venue(doublon.kiosque_1)
                kiosque_2_venue = get_kiosque_venue(doublon.kiosque_2)
                kiosque_3_venue = get_kiosque_venue(doublon.kiosque_3)
                kiosque_4_venue = get_kiosque_venue(doublon.kiosque_4)
                #kiosque_5_venue = get_kiosque_venue(doublon.kiosque_5)


            kiosque_texts = [kiosque_1_text, kiosque_2_text, kiosque_3_text,
                            kiosque_4_text]#, kiosque_5_text]

            if not all([i=="NA" for i in kiosque_texts]):
                flash('Voici tes choix','form-success')

                return render_template('main/result.html',
                                        form=form,
                                        kiosque_1_text=kiosque_1_text,
                                        kiosque_2_text=kiosque_2_text,
                                        kiosque_3_text=kiosque_3_text,
                                        kiosque_4_text=kiosque_4_text,
                                        #kiosque_5_text=kiosque_5_text,
                                        kiosque_1_venue=kiosque_1_venue,
                                        kiosque_2_venue=kiosque_2_venue,
                                        kiosque_3_venue=kiosque_3_venue,
                                        kiosque_4_venue=kiosque_4_venue)
                                        #kiosque_5_venue=kiosque_5_venue)
            else :
                flash("Tu n'as pas encore fait de choix",'form-error')

                return render_template('main/poll.html',
                                        form=form)

        return render_template('main/result.html',
                                form=form,
                                kiosque_1_text=kiosque_1_text,
                                kiosque_2_text=kiosque_2_text,
                                kiosque_3_text=kiosque_3_text,
                                kiosque_4_text=kiosque_4_text,
                                #kiosque_5_text=kiosque_5_text,
                                kiosque_1_venue=kiosque_1_venue,
                                kiosque_2_venue=kiosque_2_venue,
                                kiosque_3_venue=kiosque_3_venue,
                                kiosque_4_venue=kiosque_4_venue)
                                #kiosque_5_venue=kiosque_5_venue)

    return render_template('main/poll.html',
                            form=form)
