from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user, login_required
from flask_rq import get_queue

from app import db
from app.admin.forms import (
    ChangeAccountTypeForm,
    ChangeUserEmailForm,
    InviteUserForm,
    NewUserForm,
    ManageKiosqueForm,
    ParticipantsForm
)

from app.decorators import admin_required
from app.email import send_email
from app.models import EditableHTML, Role, User, Kiosques, Participant, get_kiosque_text
from app.utils import create_place_names

import pdb
from flask import request
from flask import Response
import pandas as pd

admin = Blueprint('admin', __name__)
cols = ["email", "kiosque_1", "presentor_1", "kiosque_2", "presentor_2",
        "kiosque_3", "presentor_3", "kiosque_4", "presentor_4"]
         #"kiosque_5", "presentor_5"]

def create_dict_participants(x, cols):

    list_participants = []

    for part in x:
        dict_participants = {cols[0]:part.email,
                            cols[1]:get_kiosque_text(part.kiosque_1),
                            cols[2]:part.presentor_1,
                            cols[3]:get_kiosque_text(part.kiosque_2),
                            cols[4]:part.presentor_2,
                            cols[5]:get_kiosque_text(part.kiosque_3),
                            cols[6]:part.presentor_3,
                            cols[7]:get_kiosque_text(part.kiosque_4),
                            cols[8]:part.presentor_4}
                            #cols[9]:get_kiosque_text(part.kiosque_5),
                            #cols[10]:part.presentor_5}
        list_participants.append(dict_participants)

    return list_participants

@admin.route('/')
@login_required
@admin_required
def index():
    """Admin dashboard page."""
    return render_template('admin/index.html')


@admin.route('/new-user', methods=['GET', 'POST'])
@login_required
@admin_required
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data,
            truite=form.truite.data,
            pintade=form.pintade.data,
            password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User {} successfully created'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@admin.route('/invite-user', methods=['GET', 'POST'])
@login_required
@admin_required
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data)
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for(
            'account.join_from_invite',
            user_id=user.id,
            token=token,
            _external=True)
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject='You Are Invited To Join',
            template='account/email/invite',
            user=user,
            invite_link=invite_link,
        )
        flash('User {} successfully invited'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@admin.route('/users')
@login_required
@admin_required
def registered_users():
    """View all registered users."""
    users = User.query.all()
    roles = Role.query.all()
    return render_template(
        'admin/registered_users.html', users=users, roles=roles)


@admin.route('/user/<int:user_id>')
@admin.route('/user/<int:user_id>/info')
@login_required
@admin_required
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/change-email', methods=['GET', 'POST'])
@login_required
@admin_required
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash('Email for user {} successfully changed to {}.'.format(
            user.full_name(), user.email), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route(
    '/user/<int:user_id>/change-account-type', methods=['GET', 'POST'])
@login_required
@admin_required
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash('You cannot change the type of your own account. Please ask '
              'another administrator to do this.', 'error')
        return redirect(url_for('admin.user_info', user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash('Role for user {} successfully changed to {}.'.format(
            user.full_name(), user.role.name), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route('/user/<int:user_id>/delete')
@login_required
@admin_required
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/_delete')
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash('You cannot delete your own account. Please ask another '
              'administrator to do this.', 'error')
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash('Successfully deleted user %s.' % user.full_name(), 'success')
    return redirect(url_for('admin.registered_users'))


@admin.route('/_update_editor_contents', methods=['POST'])
@login_required
@admin_required
def update_editor_contents():
    """Update the contents of an editor."""

    edit_data = request.form.get('edit_data')
    editor_name = request.form.get('editor_name')

    editor_contents = EditableHTML.query.filter_by(
        editor_name=editor_name).first()
    if editor_contents is None:
        editor_contents = EditableHTML(editor_name=editor_name)
    editor_contents.value = edit_data

    db.session.add(editor_contents)
    db.session.commit()

    return 'OK', 200

@admin.route('/participants', methods=['POST','GET'])
@login_required
@admin_required
def participants():
    form = ParticipantsForm()
    participants = Participant.query.all()

    dict_participants = create_dict_participants(participants, cols)

    """View all registered users."""

    if form.validate_on_submit():

        participant_to_delete = Participant.query.filter_by(email = form.participants_all.data.email).first()
        presentor_list = [participant_to_delete.presentor_1,
                        participant_to_delete.presentor_2,
                        participant_to_delete.presentor_3,
                        participant_to_delete.presentor_4,
                        #participant_to_delete.presentor_5,
                        ]

        def re_add_places_after_suppression(kiosque_n, i):
            if kiosque_n is not None:
                if not presentor_list[i]:
                    kiosque_n.places = kiosque_n.places + 1
                    kiosque_n.name_places = create_place_names(kiosque_n.name,kiosque_n.sub_name,kiosque_n.places)

                if presentor_list[i]:
                    kiosque_n.n_presentor_occupied -= 1

                db.session.add(kiosque_n)
                db.session.commit()

        kiosque_n = Kiosques.query.filter_by(id = form.participants_all.data.kiosque_1).first()
        re_add_places_after_suppression(kiosque_n, 0)
        kiosque_n = Kiosques.query.filter_by(id = form.participants_all.data.kiosque_2).first()
        re_add_places_after_suppression(kiosque_n, 1)
        kiosque_n = Kiosques.query.filter_by(id = form.participants_all.data.kiosque_3).first()
        re_add_places_after_suppression(kiosque_n, 2)
        kiosque_n = Kiosques.query.filter_by(id = form.participants_all.data.kiosque_4).first()
        re_add_places_after_suppression(kiosque_n, 3)
        #kiosque_n = Kiosques.query.filter_by(id = form.participants_all.data.kiosque_5).first()
        #re_add_places_after_suppression(kiosque_n, 4)

        db.session.delete(participant_to_delete)
        db.session.commit()
        flash('Participant supprimé')
        participants = Participant.query.all()
        dict_participants = create_dict_participants(participants, cols)

    return render_template(
        'admin/participants.html', users=dict_participants, form=form)

@admin.route("/getPlotCSV")
def getPlotCSV():
    participants = Participant.query.all()
    list_participants=[]


    for i in range(len(participants)):
        parti = participants[i].__dict__

        list_participants.append([parti[cols[0]],
                get_kiosque_text(parti[cols[1]]),
                parti[cols[2]],
                get_kiosque_text(parti[cols[3]]),
                parti[cols[4]],
                get_kiosque_text(parti[cols[5]]),
                parti[cols[6]],
                get_kiosque_text(parti[cols[7]]),
                parti[cols[8]]])
                #get_kiosque_text(parti[cols[9]]),
                #parti[cols[10]]])

    DF_participants=pd.DataFrame(list_participants, columns=cols)
    csv = DF_participants.to_csv(header=True, index=True)

    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=liste_metapleniere_ide.csv"})

@admin.route('/manage_kiosques', methods=['POST','GET'])
@login_required
@admin_required
def manage_kiosques():
    form = ManageKiosqueForm()
    kiosques = Kiosques.query.all()

    if form.validate_on_submit():

        old_id = form.kiosques_all.data.id
        data_to_change=Kiosques.query.filter_by(id = old_id).first()

        if (form.new_name.data!=None) & (form.new_name.data!=""):
            data_to_change.sub_name=form.new_name.data

        new_number_places=form.places_disponibles.data
        old_number_places_max=data_to_change.places_max
        data_to_change.places_max=new_number_places
        data_to_change.n_presentor=form.n_presentor.data
        data_to_change.venue=form.venue.data
        data_to_change.places = data_to_change.places+(new_number_places-old_number_places_max)
        data_to_change.name_places = create_place_names(data_to_change.name,data_to_change.sub_name,data_to_change.places)
        db.session.add(data_to_change)

        db.session.commit()
        flash('Modification prise en compte','form-success' )

    return render_template(
        'admin/manage_kiosques.html', users=kiosques, form=form)
