from flask_wtf import Form
from wtforms import ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    IntegerField,
    SelectField
)
from wtforms.fields.html5 import EmailField
from wtforms.validators import (
    Email,
    EqualTo,
    InputRequired,
    Length,
    DataRequired
)
from sqlalchemy import and_
from app import db
import pdb
from app.models import Role, User, Kiosques, Participant

class ChangeUserEmailForm(Form):
    email = EmailField(
        'New email', validators=[InputRequired(),
                                 Length(1, 64),
                                 Email()])
    submit = SubmitField('Update email')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')


class ChangeAccountTypeForm(Form):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'))
    submit = SubmitField('Update role')


class InviteUserForm(Form):
    role = QuerySelectField(
        'Account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'))


    first_name = StringField(
        'First name', validators=[InputRequired(),
                                  Length(1, 64)])
    last_name = StringField(
        'Last name', validators=[InputRequired(),
                                 Length(1, 64)])

    truite = StringField(
        'Truite', validators=[InputRequired(),
                                 Length(1, 64)])
    pintade = StringField(
        'Pintade', validators=[InputRequired(),
                                 Length(1, 64)])
    email = EmailField(
        'Email', validators=[InputRequired(),
                             Length(1, 64),
                             Email()])
    submit = SubmitField('Valider')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')

class ManageKiosqueForm(Form)  :
        kiosques_all = QuerySelectField(
        'Selectionner le kiosque à modifier',
        get_label='sub_name',
        query_factory=lambda: db.session.query(Kiosques).order_by("name"))
        new_name = StringField("Nouveau sous titre")
        places_disponibles = IntegerField("Nouveau nombre de places max", default=20)
        n_presentor = IntegerField("Nouveau nombre de présentateur", default=2)
        venue = SelectField("Nouveau lieu", choices = [("1B45-A","1B45-A"),
        ("1B45-B","1B45-B"), ("1B29","1B29"), ("1B35","1B35"), ("1B37","1B37")
        , ("1B39","1B39")])
        submit = SubmitField('Valider')
        submit_2 = SubmitField('Valider')

class ParticipantsForm(Form) :
        participants_all = QuerySelectField(
        'Selectionner le participant à supprimer',
        get_label='email',
        query_factory=lambda: db.session.query(Participant).order_by("email"))
        submit = SubmitField('Supprimer')

class NewUserForm(InviteUserForm):
    password = PasswordField(
        'Password',
        validators=[
            InputRequired(),
            EqualTo('password2', 'Passwords must match.')
        ])
    password2 = PasswordField('Confirm password', validators=[InputRequired()])

    submit = SubmitField('Create')
